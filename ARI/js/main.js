

$(window).scroll(function () {
  let contentHeadHeigh = $("header").height();
  var sticky = $('header'),
    scroll = $(window).scrollTop();

  if (scroll >= contentHeadHeigh) sticky.addClass('menu-scroll');
  if (scroll == 0)
    sticky.removeClass('menu-scroll');
});
// Load srcoll bar process and load header footer page
$(document).ready(function () {
  $(".header-load").load("header.html");
  $(".footer-load").load("footer.html");
});
window.onscroll = function () { myFunction() };
function myFunction() {
  var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
  var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
  var scrolled = (winScroll / height) * 100;
  document.getElementById("myBar").style.width = scrolled + "%";
}

// Js click menu
$(".menu-mobile .open-menu").click(function () {
  $('header').addClass('show-menu');
});

$(".menu-mobile i.ti-close").click(function () {
  $('header').removeClass('show-menu');
});

$("a.btn-dat-hang-ngay").on('click', function () {
  $(".mini-cart-right").addClass("show");
  $(".full-bg").addClass("active");
  $("body").addClass("hidden");
});

$(".shopping-cart-close").on('click', function () {
  $(".mini-cart-right").removeClass("show");
  $(".full-bg").removeClass("active");
  $("body").removeClass("hidden");
});


$(".view-more-tt").click(function () {
  $('.mini-cart').addClass('show-thong-tin-xuat-hd');
});
$(".remove-view-tt").click(function () {
  $('.mini-cart').removeClass('show-thong-tin-xuat-hd');

});


$(document).ready(function () {
  $(".qr-code").click(function () {
    $(".qr-code").removeClass("show-active-qr");
    $(this).addClass("show-active-qr");
  });
});




function inVisible(element) {
  //Checking if the element is
  //visible in the viewport
  var WindowTop = $(window).scrollTop();
  var WindowBottom = WindowTop + $(window).height();
  var ElementTop = element.offset().top;
  var ElementBottom = ElementTop + element.height();
  //animating the element if it is
  //visible in the viewport
  if ((ElementBottom <= WindowBottom) && ElementTop >= WindowTop)
    animate(element);
}

function animate(element) {
  //Animating the element if not animated before
  if (!element.hasClass('ms-animated')) {
    var maxval = element.data('max');
    var html = element.html();
    element.addClass("ms-animated");
    $({
      countNum: element.html()
    }).animate({
      countNum: maxval
    }, {
      //duration 5 seconds
      duration: 5000,
      easing: 'linear',
      step: function () {
        element.html(Math.floor(this.countNum) + html);
      },
      complete: function () {
        element.html(this.countNum + html);
      }
    });
  }

}

//When the document is ready
$(function () {
  //This is triggered when the
  //user scrolls the page
  $(window).scroll(function () {
    //Checking if each items to animate are 
    //visible in the viewport
    $("h2[data-max]").each(function () {
      inVisible($(this));
    });
  })
});


/* JS Select box*/
var x, i, j, l, ll, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
l = x.length;
for (i = 0; i < l; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  ll = selElmnt.length;
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < ll; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function (e) {
      /*when an item is clicked, update the original select box,
      and the selected item:*/
      var y, i, k, s, h, sl, yl;
      s = this.parentNode.parentNode.getElementsByTagName("select")[0];
      sl = s.length;
      h = this.parentNode.previousSibling;
      for (i = 0; i < sl; i++) {
        if (s.options[i].innerHTML == this.innerHTML) {
          s.selectedIndex = i;
          h.innerHTML = this.innerHTML;
          y = this.parentNode.getElementsByClassName("same-as-selected");
          yl = y.length;
          for (k = 0; k < yl; k++) {
            y[k].removeAttribute("class");
          }
          this.setAttribute("class", "same-as-selected");
          break;
        }
      }
      h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function (e) {
    /*when the select box is clicked, close any other select boxes,
    and open/close the current select box:*/
    e.stopPropagation();
    closeAllSelect(this);
    this.nextSibling.classList.toggle("select-hide");
    this.classList.toggle("select-arrow-active");
  });
}
function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, xl, yl, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  xl = x.length;
  yl = y.length;
  for (i = 0; i < yl; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < xl; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);



// Elements Slider homepage //
$(document).ready(function () {
  $('.slider-homepage').owlCarousel({
    loop: true,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: 6000,
    smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: false
      },
      1000: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});
// Elements Slider du an da thuc hien//
$(document).ready(function () {
  $('.slider-du-an').owlCarousel({
    loop: true,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: 6000,
    smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: false
      },
      1000: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});

// Elements Slider page list //
$(document).ready(function () {
  $('.slider-page-list').owlCarousel({
    loop: true,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: 6000,
    smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: false
      },
      1000: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});
// Elements Slider mẫu banner khác //
$(document).ready(function () {
  $('.slider-other-banner').owlCarousel({
    loop: true,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: 6000,
    smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 2,
        nav: false
      },
      1000: {
        items: 3,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 3,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});
// Elements Slider show full images page detail
$(document).ready(function () {
  $('.slider-show-full-img').owlCarousel({
    loop: true,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: 6000,
    smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: false
      },
      1000: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});


// Js slider detail banner page
$(document).ready(function () {
  $("#content-slider").lightSlider({
    loop: true,
    keyPress: true
  });
  $('#image-gallery').lightSlider({
    gallery: true,
    item: 1,
    thumbItem: 9,
    slideMargin: 0,
    onSliderLoad: function () {
      $('#image-gallery').removeClass('cS-hidden');
    }
  });
});


// Js click number 
$(document).ready(function () {
  $('.minus').click(function () {
    var $input = $(this).parent().find('input');
    var count = parseInt($input.val()) - 1;
    count = count < 1 ? 1 : count;
    $input.val(count);
    $input.change();
    return false;
  });
  $('.plus').click(function () {
    var $input = $(this).parent().find('input');
    $input.val(parseInt($input.val()) + 1);
    $input.change();
    return false;
  });
});


// Elements Slider page about process
$('.carousel-item', '.show-neighbors').each(function () {
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));
}).each(function () {
  var prev = $(this).prev();
  if (!prev.length) {
    prev = $(this).siblings(':last');
  }
  prev.children(':nth-last-child(2)').clone().prependTo($(this));
});



// Input- file with file name

jQuery("#inputFile").on('change', function () {

  var file = this.files[0].name;
  var fileTitle = jQuery(this).attr("placeholder");
  if (jQuery(this).val() !== "") {
    jQuery(this).next().text(file);
  } else {
    jQuery(this).next().text(fileTitle);
  }
});



// Loading page
$(window).on('DOMContentLoaded', function () {

  if ($('.spinner-container').length) {
    $('.spinner-container').delay(600).fadeOut('500');
  }
});

// Js expand nut " xem them "
$(".expand-content-editor button").click(function () {
  $(".description-body").toggleClass("heightAuto");
});
// Js expand nut " xem them danh gia"
$(".expand-content-editor-rating button").click(function () {
  $(".content-rating").toggleClass("heightAuto");
});


// Sticky menu tab  to srcoll page
$('.menu-tab ul a').click(function () {
  var href = $(this).attr('href');
  var anchor = $(href).offset();
  window.scrollTo(anchor.left, anchor.top - 150);
  return false;
});

$(window).scroll(function () {
  let contentHeadHeigh = $(".menu-tab").height();
  var sticky = $('.menu-tab'),
    scroll = $(window).scrollTop();

  if (scroll >= contentHeadHeigh) sticky.addClass('menu-scroll');
  if (scroll == 0)
    sticky.removeClass('menu-scroll');
});
$(document).ready(function () {
  $(".menu-tab ul li a").click(function () {
    $(".menu-tab ul li a").removeClass("show-active");
    $(this).addClass("show-active");
  });
});

// Sticky menu tab  to srcoll page
$('.box-muc-luc ul a').click(function () {
  var href = $(this).attr('href');
  var anchor = $(href).offset();
  window.scrollTo(anchor.left, anchor.top - 150);
  return false;
});


// show thay doi mat khau moi
function valueChanged() {
  if ($('.check-change-mk').is(":checked"))
    $(".box-thay-doi-mk").addClass('show-box-doi-mk');
  else
    $(".box-thay-doi-mk").removeClass('show-box-doi-mk');
}
// Js clicl detail tabel
$("tr").click(function () {
  window.location = "detail-order.html";
});
